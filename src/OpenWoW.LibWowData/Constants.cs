﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenWoW.LibWowData
{
    [Flags]
    public enum Flag : uint
    {
        HasOffsetMap = 0x1,
        Unknown2 = 0x2,
        HasNonInlineIDs = 0x4,
        Unknown8 = 0x8,
        Unknown16 = 0x16,
    }

    public enum FieldType
    {
        Integer,
        Integer64,
        Integer16,
        Integer8,
        UnsignedInteger,
        UnsignedInteger16,
        UnsignedInteger8,
        Float,
        String,
    }

    public enum FieldStorageType : uint
    {
        InField = 0,
        InFieldBitPacked = 1,
        InCommonData = 2,
        InArrayDataSingle = 3,
        InArrayDataGroup = 4,
        InFieldBitPacked2 = 5,
        ForeignKey = 666,
    }

    public enum WDBType
    {
        Creature,
        GameObject,
        Quest,
    }

    public static class Hardcoded
    {
        public static Dictionary<ushort, ushort> FieldSizeMap = new Dictionary<ushort, ushort>
        {
            // (32 - n) / 8
            { 0, 4 },
            { 8, 3 },
            { 16, 2 },
            { 24, 1 },
            { 32, 0 }, // this is a special signifier for something involving FieldStorageInfo
            { 0xFFE0, 8 }, // -32 for 64-bit, ha ha ha
        };

        public static uint[] BitValues = Enumerable.Range(0, 64).Select(x => (uint)1 << x).ToArray();
    }
}
