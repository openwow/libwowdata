﻿using OpenWoW.LibWowData.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OpenWoW.LibWowData.DBCache
{
    public class DBCacheReader
    {
        public int Build { get; private set; }
        public string TableHash { get; private set; }
        public List<Record> Records { get; private set; }

        private bool HeaderHasBeenRead = false;
        private bool NonInlineId;
        private BinaryReader Reader;
        private Stream FileStream;

        private const string DBCACHE_SIGNATURE = "XFTH";
        // WOW-700: finds basic foreign characters, aka latin characters with accents and similar things. We skip a few codepoints just in
        //          case Blizzard decides to do something weird like include Unicode division/multiplication symbols.
        private static Regex foreignCharacterRegex = new Regex(@"([\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u00ff]|\p{IsCJKUnifiedIdeographs}|\p{IsCyrillic}|\p{IsCyrillicSupplement}|p{IsHangulSyllables})", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);

        public DBCacheReader(Stream fileStream, bool nonInlineId = false)
        {
            this.FileStream = fileStream;
            this.NonInlineId = nonInlineId;
        }

        public void ReadHeader()
        {
            if (HeaderHasBeenRead)
            {
                return;
            }

            Reader = new BinaryReader(FileStream);
            Reader.BaseStream.Seek(0, SeekOrigin.Begin); // skip signature and version

            string sig = new string(Reader.ReadChars(4));
            if (sig != DBCACHE_SIGNATURE)
            {
                throw new InvalidDataException(String.Format("Invalid file signature: |{0}|", sig));
            }
            Reader.ReadUInt32(); // version?
            Build = Reader.ReadInt32();
            TableHash = Reader.ReadUInt32().ToString("X8");

            HeaderHasBeenRead = true;
        }

        public void LoadFile(IEnumerable<FieldType> layout)
        {
            if (!HeaderHasBeenRead)
            {
                ReadHeader();
            }

            Records = new List<Record>();
            var layoutArray = layout.ToArray();

            // Read records until we reach the end of the stream
            long length = Reader.BaseStream.Length;
            while (Reader.BaseStream.Position < length)
            {
                // Modified header written by DBCacheSplitter!
                // - int32 dataSize
                // - uint32 rowID
                int dataSize = Reader.ReadInt32();
                uint recordID = Reader.ReadUInt32();
                if (dataSize == 0)
                {
                    continue;
                }
                if (dataSize < 0 || dataSize > 4096)
                {
                    throw new InvalidDataException($"Invalid record data size: { dataSize }");
                }

                long startPosition = Reader.BaseStream.Position;

                var columnData = new List<object>();
                for (int i = 0; i < layoutArray.Length; i++)
                {
                    if (i == 0 && NonInlineId)
                    {
                        columnData.Add((int)recordID);
                        continue;
                    }

                    long prePosition = Reader.BaseStream.Position;
                    object data = null;
                    switch (layoutArray[i])
                    {
                        case FieldType.Float:
                            data = Reader.ReadSingle();
                            break;

                        case FieldType.Integer64:
                            data = Reader.ReadInt64();
                            break;

                        case FieldType.Integer:
                        case FieldType.UnsignedInteger:
                            // Database doesn't support unsigned int, read both of these as signed
                            data = Reader.ReadInt32();
                            break;

                        case FieldType.Integer16:
                            data = (int)Reader.ReadInt16();
                            break;

                        case FieldType.UnsignedInteger16:
                            data = (int)Reader.ReadUInt16();
                            break;

                        case FieldType.Integer8:
                            data = (int)Reader.ReadSByte();
                            break;

                        case FieldType.UnsignedInteger8:
                            data = (int)Reader.ReadByte();
                            break;

                        case FieldType.String:
                            data = Reader.ReadNullTerminatedString();
                            // WOW-700: we don't want non-English entries, throw if this has any foreign characters in it
                            if (foreignCharacterRegex.IsMatch((string)data))
                            {
                                throw new InvalidDataException(String.Format("Probably not English: \"{0}\"", (string)data));
                            }
                            break;
                    }

                    columnData.Add(data);
                }
                Records.Add(new Record(recordID, columnData));

                //Console.WriteLine($"- { recordID } => { String.Join(" | ", columnData) }");

                // Seek forwards if we didn't quite read the entire row
                long bytesRead = Reader.BaseStream.Position - startPosition;
                if (bytesRead != dataSize)
                {
#if DEBUG
                    Console.WriteLine("Read {0} bytes, expected {1}!", bytesRead, dataSize);
#endif
                    Reader.BaseStream.Seek(dataSize - bytesRead, SeekOrigin.Current);
                }

            }
        }
    }
}
