﻿using OpenWoW.LibWowData.Extensions;
using System;
using System.Collections.Generic;
using System.IO;

namespace OpenWoW.LibWowData.DBCache
{
    public class DBCacheSplitter
    {
        private const string DBCACHE_SIGNATURE = "XFTH";
        private readonly HashSet<int> DBCACHE_VERSIONS = new HashSet<int> { 3, 4, 5, 6 };

        public int Build { get; private set; }
        public Dictionary<string, MemoryStream> FileData { get; private set; }

        public DBCacheSplitter(Stream stream, Dictionary<string, string> tableHashToName)
        {
            FileData = new Dictionary<string, MemoryStream>();

            var cacheReader = new BinaryReader(stream);

            string signature = new string(cacheReader.ReadChars(4));
            int version = cacheReader.ReadInt32();
            if (signature != DBCACHE_SIGNATURE || !DBCACHE_VERSIONS.Contains(version))
            {
                throw new InvalidDataException($"Invalid file signature or version: { signature } { version }");
            }

            Build = cacheReader.ReadInt32();

            //Console.WriteLine($"signature={ signature } version={ version } build={ build }");

            cacheReader.BaseStream.Seek(0, SeekOrigin.Begin);
            byte[] headerData = cacheReader.ReadBytes(12);

            // Version 5+ has a 32-character SHA256 hash on the end
            if (version >= 5)
            {
                cacheReader.BaseStream.Seek(32, SeekOrigin.Current);
            }

            // Read cache rows
            var skipped = new HashSet<string>();
            long length = cacheReader.BaseStream.Length;
            while (cacheReader.BaseStream.Position < length)
            {
                long recordStart = cacheReader.BaseStream.Position;

                // Record header:
                // - uint32 signature
                // - uint32 region?
                // - uint32 ??
                // - uint32 data size
                // - uint32 table hash
                // - uint32 record ID
                // - uint32 ??
                cacheReader.BaseStream.Seek(12, SeekOrigin.Current);

                uint dataSize = cacheReader.ReadUInt32();
                uint tableHash = cacheReader.ReadUInt32();
                string tableHashString = tableHash.ToString("X8");
                uint recordID = cacheReader.ReadUInt32();

                cacheReader.BaseStream.Seek(4, SeekOrigin.Current);

                //Console.WriteLine($"dataSize={ dataSize } tableHash={ tableHash }");

                // We can skip empty rows
                bool skip = false;
                if (dataSize == 0)
                {
                    skip = true;
                }

                // We can skip unknown tables
                if (!skip && !tableHashToName.ContainsKey(tableHashString))
                {
                    skip = true;
                    if (!skipped.Contains(tableHashString))
                    {
                        skipped.Add(tableHashString);
                        Console.WriteLine($"Skipping unknown table hash { tableHashString }");
                    }
                }

                if (skip)
                {
                    cacheReader.BaseStream.Seek(dataSize, SeekOrigin.Current);
                    continue;
                }

                string tableName = tableHashToName[tableHashString];

                // Skip back to the start of the header and read in everything
                //cacheReader.BaseStream.Seek(recordStart, SeekOrigin.Begin);
                byte[] rowData = cacheReader.ReadBytes((int)dataSize); // header is 28 bytes

                // Initialise the MemoryStream if required
                if (!FileData.ContainsKey(tableName))
                {
                    FileData[tableName] = new MemoryStream();
                    FileData[tableName].Write(headerData, 0, headerData.Length);
                    FileData[tableName].Write(tableHash.ToByteArray(), 0, 4);
                    Console.WriteLine($"Found new table: 0x{ tableHashString } => { tableName }");
                }

                // Append new data
                FileData[tableName].Write(dataSize.ToByteArray(), 0, 4);
                FileData[tableName].Write(recordID.ToByteArray(), 0, 4);
                FileData[tableName].Write(rowData, 0, rowData.Length);
            }
        }
    }
}
