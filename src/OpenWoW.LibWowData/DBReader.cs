﻿using OpenWoW.LibWowData.Extensions;
using System.Collections;
using System.IO;
using System.Text;

namespace OpenWoW.LibWowData
{
    public class DBReader : BinaryReader
    {
        private int _recordSize = 0;

        public DBReader(Stream stream) : base(stream) { }

        // End-of-record alignment
        public void SetRecordSize(int recordSize)
        {
            _recordSize = recordSize;
        }
        public void StartRecord()
        {
            Flush();
        }
        public void EndRecord()
        {
            BaseStream.Position += _recordSize;
            Flush();
        }

        // String handling
        public override string ReadString()
        {
            return ReadString(true);
        }

        public string ReadString(bool countAsFourBytes)
        {
            char c;
            ArrayList holder = new ArrayList();

            long fileLength = BaseStream.Length;

            for (long i = BaseStream.Position; i < fileLength; i++)
            {
                if ((c = (char)ReadByte()) == 0)
                {
                    break;
                }

                holder.Add((byte)c);
            }

            byte[] arr = holder.ToArray(typeof(byte)) as byte[];
            string result = Encoding.UTF8.GetString(arr);

            // Take off bytes for each inline string
            if (_recordSize > 0)
            {
                // We use arr.Length because result.Length will NOT work when UTF8 is in play
                if (countAsFourBytes)
                {
                    _recordSize -= 4;
                }
                else
                {
                    _recordSize -= (arr.Length + 1);
                }
            }

            return result;
        }

        // Bit handling
        byte bitPos = 8;
        byte curr;
        public byte GetBit()
        {
            if (bitPos == 8)
            {
                curr = ReadByte();
                if (_recordSize > 0)
                {
                    --_recordSize;
                }

                bitPos = 0;
            }
            return (byte)((curr >> bitPos++) & 0x1);
        }

        public uint GetBits(int count)
        {
            uint result = 0;
            for (int iter = 0; iter < count; ++iter)
            {
                result |= ((uint)GetBit() << iter);
            }
            return result;
        }

        public int GetIntByBits(int count)
        {
            return unchecked((int)GetBits(count));
        }

        public bool GetBool()
        {
            return GetBit() != 0;
        }
        public void Flush()
        {
            bitPos = 8;
        }

        // Variable-size handling
        public uint ReadUIntN(int size)
        {
            Flush();

            // Take off bytes for each read
            if (_recordSize > 0)
            {
                _recordSize -= size;
            }

            // Supports 1, 2, 3, and 4 byte reads
            if (size == 1)
            {
                return ReadByte();
            }
            else if (size == 2)
            {
                return ReadUInt16();
            }
            else if (size == 3)
            {
                return (uint)this.ReadUInt24();
            }
            else if (size == 4)
            {
                return ReadUInt32();
            }
            else if (size == 8)
            {
                return (uint)(ReadUInt64() & 0xFFFFFFFF);
            }
            else
            {
                return 0;
            }
        }

        public int ReadIntN(int size)
        {
            uint value = ReadUIntN(size);
            // More hackiness to return actual signed values based on size
            if (size == 1)
            {
                return (sbyte)value;
            }
            else if (size == 2)
            {
                return (short)value;
            }
            else
            {
                return (int)value;
            }
        }

        public override long ReadInt64()
        {
            Flush();

            // Take off bytes for each read
            if (_recordSize > 0)
            {
                _recordSize -= 8;
            }

            return base.ReadInt64();
        }

        public override float ReadSingle()
        {
            Flush();

            // Take off bytes for each read
            if (_recordSize > 0)
            {
                _recordSize -= 4;
            }

            return base.ReadSingle();
        }
    }
}
