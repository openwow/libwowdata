﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OpenWoW.LibWowData.Extensions
{
    public static class BinaryReaderExtensions
    {
        /// <summary>
        /// Reads a 24-bit signed integer from the current stream and advances the current position of the stream by 3.
        /// </summary>
        /// <param name="br">A BinaryReader object</param>
        /// <returns></returns>
        public static int ReadInt24(this BinaryReader br)
        {
            return new Int24(br.ReadBytes(3)).Value;
        }

        /// <summary>
        /// Reads a 24-bit unsigned integer from the current stream and advances the current position of the stream by 3.
        /// </summary>
        /// <param name="br">A BinaryReader object</param>
        /// <returns></returns>
        public static int ReadUInt24(this BinaryReader br)
        {
            return new UInt24(br.ReadBytes(3)).Value;
        }

        public static string ReadNullTerminatedString(this BinaryReader br)
        {
            byte value;
            var holder = new List<byte>();
            while (true)
            {
                if ((value = br.ReadByte()) == 0)
                {
                    break;
                }
                holder.Add(value);
            }
            return Encoding.UTF8.GetString(holder.ToArray());
        }

        public static string ReadFixedLengthString(this BinaryReader br, uint bytes)
        {
            var holder = new ArrayList();
            uint lastByte = bytes - 1;
            for (uint i = 0; i < bytes; i++)
            {
                byte value = br.ReadByte();
                if (!(i == lastByte && value == 0))
                {
                    holder.Add(value);
                }
            }
            byte[] arr = holder.ToArray(typeof(byte)) as byte[];
            return Encoding.UTF8.GetString(arr);
        }

        public static uint GetBits(this BitArray ba, int start, uint bits, bool rightToLeft = false)
        {
            uint ret = 0;
            for (byte i = 0; i < bits; i++)
            {
                if (ba.Get(start + i))
                {
                    ret += Hardcoded.BitValues[rightToLeft ? bits - i - 1 : i];
                }
            }
            return ret;
        }

        public static uint GetBits(this BitArray ba, int start, int bits, bool rightToLeft = false)
        {
            return ba.GetBits(start, (uint)bits, rightToLeft);
        }
    }

    public static class FieldTypeExtensions
    {
        public static bool IsInteger(this FieldType type)
        {
            return (type == FieldType.Integer || type == FieldType.Integer16 || type == FieldType.Integer8);
        }

        public static bool IsUnsignedInteger(this FieldType type)
        {
            return (type == FieldType.UnsignedInteger || type == FieldType.UnsignedInteger16 || type == FieldType.UnsignedInteger8);
        }
    }

    public static class IntExtensions
    {
        public static byte[] ToByteArray(this uint value)
        {
            var ret = new byte[4];
            ret[0] = (byte)value;
            ret[1] = (byte)(value >> 8);
            ret[2] = (byte)(value >> 16);
            ret[3] = (byte)(value >> 24);
            return ret;
        }
    }
}
