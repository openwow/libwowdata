﻿namespace OpenWoW.LibWowData
{
    public class FieldLayout
    {
        public short Size { get; private set; }
        public short Offset { get; private set; }
        public byte ArraySize { get; internal set; }
        public FieldStorageInfo StorageInfo { get; set; }

        public FieldLayout(int size, int offset, byte arraySize = 0)
        {
            this.Size = (short)size;
            this.Offset = (short)offset;
            this.ArraySize = arraySize;
            this.StorageInfo = new FieldStorageInfo();
        }
    }
}
