﻿using System;

namespace OpenWoW.LibWowData
{
    public class FieldStorageInfo
    {
        public ushort FieldOffsetBits;
        public ushort FieldSizeBits;
        public uint ExternalDataSize;
        public FieldStorageType StorageType;
        public uint DataValue; // default value OR bitpacked offset in bits
        public uint BitPackedSizeBits;
        public uint ArrayCount;
        public uint ExternalDataOffset;
        public uint ExternalDataOffsetArray;

        public override string ToString()
        {
            return String.Format("FieldOffsetBits={0} FieldSizeBits={1} ExternalDataSize={2} StorageType={3} DataValue={4} BitPackedSizeBits={5} ArrayCount={6}",
                FieldOffsetBits, FieldSizeBits, ExternalDataSize, StorageType, DataValue, BitPackedSizeBits, ArrayCount);
        }
    }
}
