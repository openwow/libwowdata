﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OpenWoW.LibWowData
{
    public class Header
    {
        public string Version { get; private set; }
        public uint TotalRecordCount { get; internal set; }
        public uint FieldCountFromFile { get; private set; }
        public uint RecordSize { get; private set; }
        public uint TotalStringTableSize { get; private set; }
        public uint TableHash { get; private set; }
        public uint LayoutHash { get; private set; }
        public uint MinID { get; private set; }
        public uint MaxID { get; private set; }
        public int Locale { get; private set; }
        public uint DuplicateIdBlockSize { get; private set; }
        public uint Flags { get; private set; }
        public ushort IdIndex { get; private set; }
        // WDB6
        public uint TotalFieldCountFromFile { get; private set; }
        public uint CommonDataSize { get; private set; }
        // WDC1
        public uint FieldStorageInfoSize { get; private set; }
        public uint ArrayDataSize { get; private set; }
        public uint BitpackedDataOffset { get; private set; }
        public uint LookupColumnCount { get; private set; }

        // WDC2
        public List<HeaderSection> Sections { get; private set; }

        // Computed things
        public uint HeaderSize { get; private set; }

        public uint FieldCount { get; private set; }
        public uint RecordPadding { get; private set; }
        public uint RecordOffset { get; private set; }
        public uint StringOffset { get; private set; }
        public uint CommonDataOffset { get; private set; }
        public uint NonInlineIdOffset { get; private set; }
        public uint DuplicateIdOffset { get; private set; }
        public uint FieldStorageInfoOffset { get; private set; }
        public uint ArrayDataOffset { get; private set; }
        public uint ForeignKeyMapOffset { get; private set; }
        public uint IdListOffset { get; private set; }

        public List<FieldLayout> Fields { get; private set; }
        public List<FieldStorageInfo> FieldStorages { get; private set; }

        public bool HasNonInlineIDs => (Flags & (uint)Flag.HasNonInlineIDs) != 0;
        public bool HasOffsetMap => (Flags & (uint)Flag.HasOffsetMap) != 0;

        public Header(BinaryReader br)
        {
            br.BaseStream.Seek(0, SeekOrigin.Begin);

            // Read header fields
            Version = new string(br.ReadChars(4));
            TotalRecordCount = br.ReadUInt32();
            FieldCountFromFile = br.ReadUInt32();
            RecordSize = br.ReadUInt32();
            TotalStringTableSize = br.ReadUInt32();
            TableHash = br.ReadUInt32();
            LayoutHash = br.ReadUInt32();
            MinID = br.ReadUInt32();
            MaxID = br.ReadUInt32();
            Locale = br.ReadInt32();
            Flags = br.ReadUInt16();
            IdIndex = br.ReadUInt16();
            TotalFieldCountFromFile = br.ReadUInt32();
            BitpackedDataOffset = br.ReadUInt32();
            LookupColumnCount = br.ReadUInt32();
            FieldStorageInfoSize = br.ReadUInt32();
            CommonDataSize = br.ReadUInt32();
            ArrayDataSize = br.ReadUInt32();

            // WDC2 introduces sections, fun
            Sections = new List<HeaderSection>();
            uint sectionCount = br.ReadUInt32();
            for (uint i = 0; i < sectionCount; i++)
            {
                var section = new HeaderSection();
                section.Read(this, br);
                Sections.Add(section);
            }

            // Read field information
            Fields = new List<FieldLayout>();
            FieldLayout lastField = null;

            uint trueFieldCount = Math.Max(FieldCountFromFile, TotalFieldCountFromFile);
            ushort lastSizeBytes = 0;
            ushort lastSize = 0;
            ushort lastPosition = 0;
            for (int i = 0; i < FieldCountFromFile; i++)
            {
                ushort size = br.ReadUInt16();
                ushort position = br.ReadUInt16();
                if (!Hardcoded.FieldSizeMap.TryGetValue(size, out ushort sizeBytes))
                {
                    Console.WriteLine("WARN: field {0} claims to be unknown size={1} position={2}", i, size, position);
                    continue;
                }

                // If we've read at least one previous field, check for array expansion
                if (i > 0 && lastSizeBytes > 0)
                {
                    byte arraySize = (byte)((position - lastPosition) / lastSizeBytes);
                    if (arraySize > 1)
                    {
                        lastField.ArraySize = arraySize;
                        for (int j = 1; j < arraySize; j++)
                        {
                            Fields.Add(new FieldLayout(lastSizeBytes, (ushort)(lastPosition + (j * lastSizeBytes)), arraySize));
                        }
                    }
                }

                lastField = new FieldLayout(sizeBytes, position);
                Fields.Add(lastField);

                lastSize = size;
                lastSizeBytes = sizeBytes;
                lastPosition = position;
            }

            // If there are leftover bytes in the record size, add as many of the last column as will fit. This space is either padding
            // or trailing arrays, worst case is we get a bunch of 0 columns.
            int left = (int)(RecordSize - lastPosition - lastSizeBytes);
            if (lastField != null && lastSizeBytes > 0 && left > 0)
            {
                RecordPadding = (uint)(left % lastSizeBytes);
                uint extraCount = (uint)(left / lastSizeBytes);
                lastField.ArraySize = (byte)(1 + extraCount);
                //Console.WriteLine("recordPadding={0} extraCount={1} left={2} lastSizeBytes={3}", RecordPadding, extraCount, left, lastSizeBytes);
                for (int i = 1; i <= extraCount; i++)
                {
                    Fields.Add(new FieldLayout(lastSizeBytes, (ushort)(lastPosition + (i * lastSizeBytes)), (byte)(1 + extraCount)));
                }
            }

            FieldCount = (uint)Fields.Count;
            HeaderSize = (uint)br.BaseStream.Position;

            // Calculate some things we'll probably need
            StringOffset = HeaderSize + (TotalRecordCount * RecordSize);
            FieldStorageInfoOffset = HeaderSize;
            ArrayDataOffset = FieldStorageInfoOffset + FieldStorageInfoSize;
            CommonDataOffset = ArrayDataOffset + ArrayDataSize;
            RecordOffset = CommonDataOffset + CommonDataSize;

            if (HasOffsetMap)
            {
                TotalStringTableSize = 0; // We will force this to be zero in case somehow it is not
            }
            else
            {
                StringOffset = RecordOffset + (TotalRecordCount * RecordSize);
            }

            NonInlineIdOffset = StringOffset + TotalStringTableSize;
            DuplicateIdOffset = NonInlineIdOffset + (uint)Sections.Sum(x => x.IdListSize);
            ForeignKeyMapOffset = DuplicateIdOffset + DuplicateIdBlockSize;


            FieldStorages = new List<FieldStorageInfo>();
            int fieldIter = 0;
            uint arrayOffset = 0, commonOffset = 0;
            br.BaseStream.Seek(FieldStorageInfoOffset, SeekOrigin.Begin);
            uint numFieldStorages = (FieldStorageInfoSize / 24);
            for (uint i = 0; i < numFieldStorages; i++)
            {
                var info = new FieldStorageInfo();
                info.FieldOffsetBits = br.ReadUInt16();
                info.FieldSizeBits = br.ReadUInt16();
                info.ExternalDataSize = br.ReadUInt32();
                info.StorageType = (FieldStorageType)br.ReadUInt32();
                info.DataValue = br.ReadUInt32();
                info.BitPackedSizeBits = br.ReadUInt32();
                info.ArrayCount = br.ReadUInt32();

                if (info.StorageType == FieldStorageType.InCommonData)
                {
                    info.ExternalDataOffset = commonOffset;
                    commonOffset += info.ExternalDataSize;
                }
                else if (info.StorageType == FieldStorageType.InArrayDataSingle || info.StorageType == FieldStorageType.InArrayDataGroup)
                {
                    info.ExternalDataOffset = arrayOffset;
                    info.ExternalDataOffsetArray = arrayOffset / 4;
                    arrayOffset += info.ExternalDataSize;
                }

                FieldStorages.Add(info);

                // Fix array sizes
                if (info.StorageType == FieldStorageType.InArrayDataGroup)
                {
                    Fields[fieldIter].StorageInfo = info;
                    Fields[fieldIter].ArraySize = (byte)info.ArrayCount;
                    short size = Fields[fieldIter].Size;
                    short offset = Fields[fieldIter].Offset;
                    ++fieldIter;

                    // Expand arrays and recalc field count
                    for (int j = 1; j < info.ArrayCount; ++j)
                    {
                        Fields.Insert(fieldIter, new FieldLayout(size, offset, (byte)info.ArrayCount));
                        Fields[fieldIter].StorageInfo = info;
                        ++fieldIter;
                    }
                    FieldCount = (uint)Fields.Count;
                }
                else if (info.StorageType == FieldStorageType.InFieldBitPacked || info.StorageType == FieldStorageType.InFieldBitPacked2 || info.StorageType == FieldStorageType.InCommonData || info.StorageType == FieldStorageType.InArrayDataSingle)
                {
                    // Assign StorageInfo to Field
                    Fields[fieldIter].StorageInfo = info;
                    Fields[fieldIter].ArraySize = 1;
                    ++fieldIter;
                }
                // Update InField fields
                else
                {
                    // Fix final array size if possible
                    if ((i + 1 == numFieldStorages) && info.StorageType == FieldStorageType.InField && Fields[Fields.Count - 1].ArraySize > 1)
                    {
                        int realArraySize = info.FieldSizeBits / (Fields[Fields.Count - 1].Size * 8);
                        int fieldDifference = Fields[Fields.Count - 1].ArraySize - realArraySize;
                        if (fieldDifference > 0)
                        {
                            // Kill extra fields, fix array size variable, and recalc field count
                            for (int j = 0; j < fieldDifference; ++j)
                            {
                                Fields.RemoveAt(Fields.Count - 1);
                            }
                            for (int j = 0; j < realArraySize; ++j)
                            {
                                Fields[Fields.Count - 1 - j].ArraySize = (byte)realArraySize;
                            }
                            FieldCount = (uint)Fields.Count;
                        }
                    }

                    // Assign StorageInfo to Fields
                    Fields[fieldIter].StorageInfo = info;
                    int arraySize = Fields[fieldIter].ArraySize;
                    ++fieldIter;
                    for (int j = 1; j < arraySize; ++j)
                    {
                        Fields[fieldIter].StorageInfo = info;
                        ++fieldIter;
                    }
                }
            }

            // If the foreign key map has any data we need a fake column for it
            if (Sections.Any(x => x.ForeignKeyMapSize > 0))
            {
                var fkLayout = new FieldLayout(4, -1);
                fkLayout.StorageInfo.StorageType = FieldStorageType.ForeignKey;
                Fields.Add(fkLayout);
            }
        }

        public bool IsWDC2 => Version == "WDC2";
        public bool IsWDC3 => Version == "WDC3";

        public bool HasForeignKeyMap => Sections.Any(x => x.ForeignKeyMapSize > 0);

        public string TableHashHex => TableHash.ToString("X8");
        public string LayoutHashHex => LayoutHash.ToString("X8");
    }
}
