﻿using System;
using System.IO;

namespace OpenWoW.LibWowData
{
    public class HeaderSection
    {
        public UInt64 TactKeyHash { get; private set; }
        public uint CopyTableCount { get; private set; }
        public uint CopyTableSize { get; private set; }
        public uint FileOffset { get; private set; }
        public uint ForeignKeyMapSize { get; private set; }
        public uint IdListSize { get; private set; }
        public uint RecordCount { get; internal set; }
        public uint SparseTableCount { get; private set; }
        public uint SparseTableOffset { get; private set; } // Start of the offset map
        public uint StringTableSize { get; private set; }

        // Calculated
        public uint AbsoluteCopyTablePosition { get; private set; }
        public uint AbsoluteForeignKeyMapPosition { get; private set; }
        public uint AbsoluteIdListPosition { get; private set; }
        public uint AbsoluteOffsetMapPosition { get; private set; }
        public uint AbsoluteOffsetMapIdListPosition { get; private set; }
        public uint AbsoluteStringPosition { get; private set; }

        internal void Read(Header header, BinaryReader br)
        {
            TactKeyHash = br.ReadUInt64();
            FileOffset = br.ReadUInt32();
            RecordCount = br.ReadUInt32();
            StringTableSize = br.ReadUInt32();

            if (header.IsWDC2)
            {
                SparseTableCount = header.MaxID - header.MinID + 1;
                CopyTableSize = br.ReadUInt32();
            }

            SparseTableOffset = br.ReadUInt32();
            IdListSize = br.ReadUInt32();
            ForeignKeyMapSize = br.ReadUInt32();

            if (header.IsWDC3)
            {
                SparseTableCount = br.ReadUInt32();
                uint copyTableCount = br.ReadUInt32();
                CopyTableSize = copyTableCount * 8;
            }

            // Calculate things
            if (header.HasOffsetMap)
            {
                StringTableSize = 0;
                AbsoluteStringPosition = FileOffset;

                if (header.IsWDC2)
                {
                    AbsoluteOffsetMapPosition = SparseTableOffset;
                    AbsoluteIdListPosition = AbsoluteOffsetMapPosition + (SparseTableCount * 6);
                    AbsoluteCopyTablePosition = AbsoluteIdListPosition + IdListSize;
                    AbsoluteForeignKeyMapPosition = AbsoluteCopyTablePosition + CopyTableSize;
                }
                else
                {
                    AbsoluteIdListPosition = SparseTableOffset;
                    AbsoluteCopyTablePosition = AbsoluteIdListPosition + IdListSize;
                    AbsoluteOffsetMapPosition = AbsoluteCopyTablePosition + CopyTableSize;
                    AbsoluteForeignKeyMapPosition = AbsoluteOffsetMapPosition + (SparseTableCount * 6);
                    AbsoluteOffsetMapIdListPosition = AbsoluteForeignKeyMapPosition + ForeignKeyMapSize;
                }
            }
            else
            {
                SparseTableCount = 0;
                AbsoluteStringPosition = FileOffset + (header.RecordSize * RecordCount);
                AbsoluteIdListPosition = AbsoluteStringPosition + StringTableSize;
                AbsoluteCopyTablePosition = AbsoluteIdListPosition + IdListSize;
                AbsoluteOffsetMapPosition = AbsoluteCopyTablePosition + CopyTableSize;
                AbsoluteForeignKeyMapPosition = AbsoluteOffsetMapPosition;
                AbsoluteOffsetMapIdListPosition = AbsoluteForeignKeyMapPosition;
            }

            //Console.WriteLine($"-- string={AbsoluteStringOffset} idList={AbsoluteIdListOffset} copyTable={AbsoluteCopyTableOffset} sparseData={AbsoluteSparseDataOffset} foreignKey={AbsoluteForeignKeyMapOffset}");

            /* WDC2:
             *   [
             *     record data
             *     string data
             *     -OR-
             *     sparse record data
             *     offset map
             *   ]
             *   id list
             *   copy table
             *   foreign key map
             * 
             * WDC3:
             *   [
             *     record data
             *     string data
             *     -OR-
             *     sparse record data
             *   ]
             *   id list
             *   copy table
             *   offset map
             *   foreign key map
             */
        }
    }
}
