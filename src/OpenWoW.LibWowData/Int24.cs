﻿namespace OpenWoW.LibWowData
{
    /// <summary>
    /// Represents a 24-bit signed integer
    /// </summary>
    public class Int24
    {
        protected byte b0;
        protected byte b1;
        protected byte b2;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes">An array of 3 bytes in little endian order</param>
        public Int24(byte[] bytes)
        {
            if (bytes.Length == 3)
            {
                b0 = bytes[0];
                b1 = bytes[1];
                b2 = bytes[2];
            }
        }

        public int Value
        {
            get
            {
                return (int)(b0 | (b1 << 8) | ((b2 & 0x7F) << 16) | ((b2 & 0x80) << 24));
            }
        }
    }

    /// <summary>
    /// Represents a 24-bit unsigned integer
    /// </summary>
    public class UInt24 : Int24
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes">An array of 3 bytes in little endian order</param>
        public UInt24(byte[] bytes) : base(bytes)
        { }

        new public int Value
        {
            get
            {
                return (int)(b0 | (b1 << 8) | (b2 << 16));
            }
        }
    }
}
