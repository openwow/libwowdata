﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.LibWowData
{
    public class Record
    {
        public uint ID { get; set; }
        public List<object> Fields { get; private set; }

        public Record(uint id, List<object> fields)
        {
            this.ID = id;
            this.Fields = fields;
        }

        public Record(List<object> fields) : this(0, fields)
        { }
    }
}
