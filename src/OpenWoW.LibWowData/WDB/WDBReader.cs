﻿using OpenWoW.LibWowData.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OpenWoW.LibWowData.WDB
{
    /// <summary>
    /// A simple WDB reader for reading current formats only.
    /// </summary>
    public class WDBReader
    {
        private int PatchVersion;
        private Stream FileStream;

        private BinaryReader Reader;
        public List<Dictionary<string, object>> RecordData { get; private set; }
        public WDBType Type { get; private set; }

        #region Constants
        private const string SIGNATURE_CREATURE = "BOMW";
        private const string SIGNATURE_OBJECT = "BOGW";
        private const string SIGNATURE_QUEST = "TSQW";
        private static char[] PATCH_SPLIT = new char[] { '.' };

        private static byte[] BitReverseTable = {
            0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
            0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
            0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
            0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
            0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
            0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
            0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
            0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
            0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
            0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
            0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
            0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
            0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
            0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
            0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
            0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
            0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
            0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
            0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
            0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
            0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
            0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
            0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
            0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
            0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
            0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
            0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
            0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
            0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
            0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
            0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
            0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff
        };
        #endregion

        public WDBReader(int patchVersion, Stream fileStream)
        {
            PatchVersion = patchVersion;
            FileStream = fileStream;

            RecordData = new List<Dictionary<string, object>>();
        }

        public WDBReader(string patchString, Stream fileStream) : this(ParsePatchString(patchString), fileStream)
        { }

        public WDBReader(int patchVersion, string fileName) : this(patchVersion, File.OpenRead(fileName))
        { }

        public WDBReader(string patchString, string fileName) : this(patchString, File.OpenRead(fileName))
        { }

        private static int ParsePatchString(string patchString)
        {
            int patchValue = -1;
            string[] parts = patchString.Split('.');
            if (parts.Length == 3)
            {
                if (int.TryParse(parts[0], out int major) && int.TryParse(parts[1], out int minor) && int.TryParse(parts[2], out int patch))
                {
                    patchValue = (major * 10000) + (minor * 100) + patch;
                }
            }
            if (patchValue == -1)
            {
                throw new ArgumentException($"Not a valid patch string!", nameof(patchString));
            }
            return patchValue;
        }

        public void LoadFile()
        {
            Reader = new BinaryReader(FileStream);
            Reader.BaseStream.Seek(0, SeekOrigin.Begin);

            string signature = new string(Reader.ReadChars(4));
            uint build = Reader.ReadUInt32();
            string locale = new string(Reader.ReadChars(4));
            if (locale != "SUne")
            {
                throw new InvalidDataException($"Invalid WDB locale: { locale }");
            }

            Reader.ReadInt32(); // unk1
            Reader.ReadInt32(); // unk2
            Reader.ReadInt32(); // unk3

            Console.WriteLine($"signature={ signature } build={ build } locale={ locale }");

            Action<Dictionary<string, object>> recordFunc = null;
            switch (signature)
            {
                case SIGNATURE_CREATURE:
                    recordFunc = LoadCreatureRecord;
                    Type = WDBType.Creature;
                    break;

                case SIGNATURE_OBJECT:
                    recordFunc = LoadGameObjectRecord;
                    Type = WDBType.GameObject;
                    break;

                case SIGNATURE_QUEST:
                    recordFunc = LoadQuestRecord;
                    Type = WDBType.Quest;
                    break;

                default:
                    throw new Exception($"Signature { signature } unknown!");
            }

            long finishPosition = Reader.BaseStream.Length - 8;
            while (Reader.BaseStream.Position < finishPosition)
            {
                int id = Reader.ReadInt32();
                int length = Reader.ReadInt32();
                if (length > 0)
                {
                    long startPosition = Reader.BaseStream.Position;
                    long expectedEndPosition = startPosition + length;

                    var record = new Dictionary<string, object> { { "ID", id } };
                    recordFunc(record);

                    /*Console.WriteLine("---");
                    Console.WriteLine($"Record { id }");
                    foreach (var kvp in record)
                    {
                        Console.WriteLine($"- { kvp.Key } => { kvp.Value }");
                    }*/

                    long endPosition = Reader.BaseStream.Position;
                    if (endPosition != expectedEndPosition)
                    {
                        throw new Exception($"Format change? Read { endPosition - startPosition }, expected { expectedEndPosition - startPosition } for record ID={ id } @ 0x{ (startPosition - 8).ToString("X") }");
                    }

                    RecordData.Add(record);
                }
            }
        }

        private void LoadCreatureRecord(Dictionary<string, object> record)
        {
            // String lengths are stored as a bit-packed array instead of just using null-terminated strings, fun times
            byte[] lengthBytes = Reader.ReadBytes(15);
            BitArray ba = new BitArray(lengthBytes.Select(x => BitReverseTable[x]).ToArray());
            uint titleLength = ba.GetBits(0, 11, true);
            uint titleAltLength = ba.GetBits(11, 11, true);
            uint cursorNameLength = ba.GetBits(22, 6, true);
            record["IsFactionLeader"] = ba.GetBits(28, 1);
            uint name0Length = ba.GetBits(29, 11, true);
            uint name0AltLength = ba.GetBits(40, 11, true);
            uint name1Length = ba.GetBits(51, 11, true);
            uint name1AltLength = ba.GetBits(62, 11, true);
            uint name2Length = ba.GetBits(73, 11, true);
            uint name2AltLength = ba.GetBits(84, 11, true);
            uint name3Length = ba.GetBits(95, 11, true);
            uint name3AltLength = ba.GetBits(106, 11, true);

            record["Name0"] = Reader.ReadFixedLengthString(name0Length);
            record["Name0Alt"] = Reader.ReadFixedLengthString(name0AltLength);
            record["Name1"] = Reader.ReadFixedLengthString(name1Length);
            record["Name1Alt"] = Reader.ReadFixedLengthString(name1AltLength);
            record["Name2"] = Reader.ReadFixedLengthString(name2Length);
            record["Name2Alt"] = Reader.ReadFixedLengthString(name2AltLength);
            record["Name3"] = Reader.ReadFixedLengthString(name3Length);
            record["Name3Alt"] = Reader.ReadFixedLengthString(name3AltLength);

            record["Flags"] = Reader.ReadInt32();
            record["Flags2"] = Reader.ReadInt32();
            record["CreatureType"] = Reader.ReadInt32();
            record["CreatureFamily"] = Reader.ReadInt32();
            record["Classification"] = Reader.ReadInt32();
            record["ProxyCreature0_ID"] = Reader.ReadInt32();
            record["ProxyCreature1_ID"] = Reader.ReadInt32();

            int numCreatureDisplays = Reader.ReadInt32();

            record["UNK_BFA_Multiplier"] = Reader.ReadSingle();

            for (int i = 0; i < numCreatureDisplays; i++)
            {
                record[$"CreatureDisplay{ i }_ID"] = Reader.ReadInt32();
                record[$"CreatureDisplay{ i }_Scale"] = Reader.ReadSingle();
                record[$"CreatureDisplay{ i }_Probability"] = Reader.ReadSingle();
            }

            record["HPMultiplier"] = Reader.ReadSingle();
            record["EnergyMultiplier"] = Reader.ReadSingle();

            uint numQuestItems = Reader.ReadUInt32();

            record["CreatureMovementInfoID"] = Reader.ReadInt32();
            record["RequiredExpansion"] = Reader.ReadInt32();
            record["TrackingQuestID"] = Reader.ReadInt32();
            record["LEGION_INT_1"] = Reader.ReadInt32();
            record["LEGION_INT_2"] = Reader.ReadInt32();

            if (PatchVersion >= 80100)
            {
                record["28202_INT_1"] = Reader.ReadInt32();
            }

            record["Title"] = Reader.ReadFixedLengthString(titleLength);
            record["TitleAlt"] = Reader.ReadFixedLengthString(titleAltLength);
            // cursorNameLength is set to 1 when it actually means 0, for some reason
            if (cursorNameLength != 1)
            {
                record["Cursor"] = Reader.ReadFixedLengthString(cursorNameLength);
            }
            else
            {
                record["Cursor"] = "";
            }

            for (uint i = 0; i < numQuestItems; i++)
            {
                record[$"QuestItem{ i }_ID"] = Reader.ReadInt32();
            }
        }

        private void LoadGameObjectRecord(Dictionary<string, object> record)
        {
            record["Type"] = Reader.ReadInt32();
            record["GameObjectDisplayInfoID"] = Reader.ReadInt32();
            record["Name"] = Reader.ReadNullTerminatedString();
            record["Name2"] = Reader.ReadNullTerminatedString();
            record["Name3"] = Reader.ReadNullTerminatedString();
            record["Name4"] = Reader.ReadNullTerminatedString();
            record["Icon"] = Reader.ReadNullTerminatedString();
            record["Action"] = Reader.ReadNullTerminatedString();
            record["Condition"] = Reader.ReadNullTerminatedString();

            int dataCounter = 0;
            for (; dataCounter < 34; dataCounter++)
            {
                record[$"Data_{ dataCounter }"] = Reader.ReadInt32();
            }

            record["Scale"] = Reader.ReadSingle();

            byte numQuestItems = Reader.ReadByte();
            for (int i = 0; i < numQuestItems; i++)
            {
                record[$"QuestItem{ i }_ID"] = Reader.ReadInt32();
            }

            record["MinLevel"] = Reader.ReadInt32();
        }

        private void LoadQuestRecord(Dictionary<string, object> record)
        {
            record["ID2"] = Reader.ReadInt32();
            record["Type"] = Reader.ReadInt32();
            record["Level"] = Reader.ReadInt32();
            record["Quest_UNK_27075"] = Reader.ReadInt32();
            record["MaxScalingLevel"] = Reader.ReadInt32();
            record["PackageID"] = Reader.ReadInt32();
            record["MinLevel"] = Reader.ReadInt32();
            record["SortID"] = Reader.ReadInt32();
            record["InfoID"] = Reader.ReadInt32();
            record["SuggestedGroupNum"] = Reader.ReadInt32();
            record["RewardNextQuest"] = Reader.ReadInt32();
            record["RewardXPDifficulty"] = Reader.ReadInt32();
            record["RewardXPMultiplier"] = Reader.ReadSingle();
            record["RewardMoney"] = Reader.ReadInt32();
            record["RewardMoneyDifficulty"] = Reader.ReadInt32();
            record["RewardMoneyMultiplier"] = Reader.ReadSingle();
            record["RewardBonusMoney"] = Reader.ReadInt32();
            record["RewardDisplaySpell0_ID"] = Reader.ReadInt32();
            record["RewardDisplaySpell1_ID"] = Reader.ReadInt32();
            record["RewardDisplaySpell2_ID"] = Reader.ReadInt32();
            record["RewardSpell"] = Reader.ReadInt32();
            record["RewardHonorAddition"] = Reader.ReadInt32();
            record["RewardHonorMultiplier"] = Reader.ReadSingle();
            record["RewardArtifactXPDifficulty"] = Reader.ReadInt32();
            record["RewardArtifactXPMultiplier"] = Reader.ReadSingle();
            record["RewardArtifactCategoryID"] = Reader.ReadInt32();
            record["ProvidedItem"] = Reader.ReadInt32();
            record["Flags"] = Reader.ReadInt32();
            record["Flags2"] = Reader.ReadInt32();
            record["BFA_UNK_27075"] = Reader.ReadInt32();

            for (int i = 0; i < 4; i++)
            {
                record[$"RewardFixedItems{ i }_ItemID"] = Reader.ReadInt32();
                record[$"RewardFixedItems{ i }_Quantity"] = Reader.ReadInt32();
            }

            for (int i = 0; i < 4; i++)
            {
                record[$"ItemDrop{ i }_ItemID"] = Reader.ReadInt32();
                record[$"ItemDrop{ i }_Quantity"] = Reader.ReadInt32();
            }

            for (int i = 0; i < 6; i++)
            {
                record[$"RewardChoiceItems{ i }_ItemID"] = Reader.ReadInt32();
                record[$"RewardChoiceItems{ i }_Quantity"] = Reader.ReadInt32();
                record[$"RewardChoiceItems{ i }_DisplayID"] = Reader.ReadInt32();
            }

            record["POIContinent"] = Reader.ReadInt32();
            record["POIx"] = Reader.ReadSingle();
            record["POIy"] = Reader.ReadSingle();
            record["POIPriority"] = Reader.ReadInt32();
            record["RewardTitle"] = Reader.ReadInt32();
            record["RewardArenaPoints"] = Reader.ReadInt32();
            record["RewardSkillLineID"] = Reader.ReadInt32();
            record["RewardNumSkillUps"] = Reader.ReadInt32();
            record["PortraitGiverDisplayID"] = Reader.ReadInt32();
            record["BFA_UNK"] = Reader.ReadInt32();

            record["PortraitTurnInDisplayID"] = Reader.ReadInt32();

            for (int i = 0; i < 5; i++)
            {
                record[$"RewardFaction{ i }_FactionID"] = Reader.ReadInt32();
                record[$"RewardFaction{ i }_Value"] = Reader.ReadInt32();
                record[$"RewardFaction{ i }_Override"] = Reader.ReadInt32();
                record[$"RewardFaction{ i }_UNK"] = Reader.ReadInt32();
            }

            record["RewardFactionFlags"] = Reader.ReadInt32();

            for (int i = 0; i < 4; i++)
            {
                record[$"RewardCurrency{ i }_CurrencyID"] = Reader.ReadInt32();
                record[$"RewardCurrency{ i }_Quantity"] = Reader.ReadInt32();
            }

            record["AcceptedSoundKitID"] = Reader.ReadInt32();
            record["CompleteSoundKitID"] = Reader.ReadInt32();
            record["AreaGroupID"] = Reader.ReadInt32();
            record["TimeAllowed"] = Reader.ReadInt32();
            int numObjectives = Reader.ReadInt32();
            record["RaceFlags"] = Reader.ReadInt64();
            record["QuestRewardID"] = Reader.ReadInt32();
            record["ExpansionID"] = Reader.ReadInt32();

            // Bit-packed string lengths, yay
            byte[] lengthBytes = Reader.ReadBytes(12);
            BitArray ba = new BitArray(lengthBytes.Select(x => BitReverseTable[x]).ToArray());
            uint titleLength, summaryLength, textLength, trackerTextLength, portraitGiverTextLength, portraitGiverNameLength, portraitTurnInTextLength, portraitTurnInNameLength, completionBlurbLength;
            if (PatchVersion >= 80100)
            {
                titleLength = ba.GetBits(0, 10, true);
                summaryLength = ba.GetBits(10, 12, true);
                textLength = ba.GetBits(22, 12, true);
                trackerTextLength = ba.GetBits(34, 9, true);
                portraitGiverTextLength = ba.GetBits(43, 11, true);
                portraitGiverNameLength = ba.GetBits(54, 9, true);
                portraitTurnInTextLength = ba.GetBits(63, 11, true);
                portraitTurnInNameLength = ba.GetBits(74, 9, true);
                completionBlurbLength = ba.GetBits(83, 12, true);
            }
            else
            {
                titleLength = ba.GetBits(0, 9, true);
                summaryLength = ba.GetBits(9, 12, true);
                textLength = ba.GetBits(21, 12, true);
                trackerTextLength = ba.GetBits(33, 9, true);
                portraitGiverTextLength = ba.GetBits(42, 10, true);
                portraitGiverNameLength = ba.GetBits(52, 8, true);
                portraitTurnInTextLength = ba.GetBits(60, 10, true);
                portraitTurnInNameLength = ba.GetBits(70, 8, true);
                completionBlurbLength = ba.GetBits(78, 11, true);
            }

            for (int i = 0; i < numObjectives; i++)
            {
                record[$"Objective{ i }_ID"] = Reader.ReadInt32();
                record[$"Objective{ i }_Type"] = Reader.ReadByte();
                record[$"Objective{ i }_StorageIndex"] = Reader.ReadSByte();
                record[$"Objective{ i }_ObjectID"] = Reader.ReadInt32();
                record[$"Objective{ i }_Amount"] = Reader.ReadInt32();
                record[$"Objective{ i }_Flags"] = Reader.ReadInt32();
                record[$"Objective{ i }_Flags2"] = Reader.ReadInt32();
                record[$"Objective{ i }_PercentAmount"] = Reader.ReadSingle();
                int objNumVisualEffects = Reader.ReadInt32();
                for (int j = 0; j < objNumVisualEffects; j++)
                {
                    record[$"Objective{ i }_VisualEffect{ j }"] = Reader.ReadInt32();
                }
                byte objDescriptionLength = Reader.ReadByte();
                record[$"Objective{ i }_Description"] = Reader.ReadFixedLengthString(objDescriptionLength);
            }

            record["Title"] = Reader.ReadFixedLengthString(titleLength);
            record["Summary"] = Reader.ReadFixedLengthString(summaryLength);
            record["Text"] = Reader.ReadFixedLengthString(textLength);
            record["TrackerText"] = Reader.ReadFixedLengthString(trackerTextLength);
            record["PortraitGiverText"] = Reader.ReadFixedLengthString(portraitGiverTextLength);
            record["PortraitGiverName"] = Reader.ReadFixedLengthString(portraitGiverNameLength);
            record["PortraitTurnInText"] = Reader.ReadFixedLengthString(portraitTurnInTextLength);
            record["PortraitTurnInName"] = Reader.ReadFixedLengthString(portraitTurnInNameLength);
            record["CompletionBlurb"] = Reader.ReadFixedLengthString(completionBlurbLength);
        }
    }
}
