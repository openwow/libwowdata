﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace OpenWoW.LibWowData
{
    public class WowDataFile
    {
        private const uint ENCRYPTED_SENTINEL = 0x3F3F3F3F;

        private byte[] Bytes;
        private DBReader Reader;
        private uint[] ArrayData = null;
        private Dictionary<long, string> StringTable;
        private Dictionary<uint, List<uint>> IdLookup;
        private Dictionary<int, Dictionary<uint, object>> CommonDataLookup;
        private uint[] ForeignKeyLookup = null;
        private List<uint> NonInlineIDs = null;
        private List<uint> SparseOffsets = null;
        private List<ushort> SparseSizes = null;

        public bool Debug { get; set; }
        public int IdColumn { get; set; }

        public List<FieldType> FieldTypes { get; set; }
        public Header Header { get; private set; }
        public List<Record> Records { get; private set; }
        public long ElapsedMilliseconds { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="fields"></param>
        /// <param name="idColumn"></param>
        public WowDataFile(byte[] bytes, IEnumerable<FieldType> fields = null, int idColumn = 0)
        {
            Bytes = bytes;
            Reader = new DBReader(new MemoryStream(bytes));
            IdColumn = idColumn;
            FieldTypes = fields?.ToList();
            Header = null;
            IdLookup = new Dictionary<uint, List<uint>>();
            CommonDataLookup = new Dictionary<int, Dictionary<uint, object>>();
            Records = new List<Record>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="fields"></param>
        /// <param name="idColumn"></param>
        public WowDataFile(string filename, IEnumerable<FieldType> fields = null, int idColumn = 0)
            : this(File.ReadAllBytes(filename), fields, idColumn)
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="onlyLoadHeader">Only load the header instead of loading the entire file</param>
        public void Load(bool onlyLoadHeader = false)
        {
            var sw = Stopwatch.StartNew();

            // Load the header and WDB5 field information
            Header = new Header(Reader);
            if (Debug)
            {
                IdColumn = Header.IdIndex;
            }

            // WDB6 common data is a pain - a file can have common data with a column count of 0 for some inane reason. Load it
            // now so that we can work out if we need the stupid thing.
            LoadCommonData();

            if (!onlyLoadHeader)
            {
                if (FieldTypes != null)
                {
                    // If the file has non-inline IDs, we need to remove the provided ID field type
                    if (Header.HasNonInlineIDs)
                    {
                        FieldTypes.RemoveAt(IdColumn);
                    }

                    // Check to make sure they're not asking for more fields than we know about
                    if (FieldTypes.Count > Header.Fields.Count)
                    {
                        Console.WriteLine("Provided {0} fields, expected at most {1}", FieldTypes.Count, Header.Fields.Count);
                        throw new Exception(String.Format("Provided field types array too large! Expected <={0}, got {1}", Header.Fields.Count, FieldTypes.Count));
                    }
                }

                Records = new List<Record>((int)Header.TotalRecordCount);

                // If no field types were provided, generate enough Integer fields to pad it out.
                if (FieldTypes == null || FieldTypes.Count == 0)
                {
                    // If the file has an offset map, no fields is an error. FIXME: exception type
                    if (Header.HasOffsetMap)
                    {
                        throw new Exception("Can't use default field formats if file has offset map!");
                    }

                    FieldTypes = new List<FieldType>();
                    int checkValue = Header.Fields.Count - 1;
                    bool hasForeignKeyMap = Header.HasForeignKeyMap;
                    for (int i = 0; i < Header.Fields.Count; i++)
                    {
                        // WDC1 hack, it already adds a field for the FK
                        if (hasForeignKeyMap && i == checkValue)
                        {
                            continue;
                        }
                        FieldLayout field = Header.Fields[i];
                        int actualFieldSizeBits = field.StorageInfo.FieldSizeBits / Math.Max((byte)1, field.ArraySize);
                        if (i == Header.IdIndex)
                        {
                            FieldTypes.Add(FieldType.UnsignedInteger);
                        }
                        else
                        {
                            FieldTypes.Add(actualFieldSizeBits == 64 ? FieldType.Integer64 : FieldType.Integer);
                        }
                    }
                }

                LoadArrayData();

                for (int i = 0; i < Header.Sections.Count; i++)
                {
                    var section = Header.Sections[i];
                    // Early encrypted awfulness skip
                    if (!LoadNonInlineIDs(section) ||
                        !LoadDuplicateIDs(section) ||
                        !LoadForeignKeys(section))
                    {
                        continue;
                    }

                    // Normal files
                    if (!Header.HasOffsetMap)
                    {
                        LoadStringTable(section);
                        LoadData(section);
                    }
                    // Evil files (offset map)
                    else
                    {
                        LoadOffsetMap(section);
                        LoadSparseData(section);
                    }
                }

                // If the file has non-inline IDs, fix the FieldTypes by adding a fake ID column
                if (Header.HasNonInlineIDs)
                {
                    FieldTypes.Insert(IdColumn, FieldType.UnsignedInteger);
                }

                // If the file has foreign key data, fix the FieldTypes by adding a new column to the end
                if (Header.HasForeignKeyMap && FieldTypes.Count < Header.Fields.Count)
                {
                    FieldTypes.Add(FieldType.UnsignedInteger);
                }

                // Forcibly clean up
                Reader.Close();
                Reader = null;
                StringTable = null;
                IdLookup = null;
                CommonDataLookup = null;
                ForeignKeyLookup = null;
                NonInlineIDs = null;
                SparseOffsets = null;
                SparseSizes = null;
            }

            sw.Stop();
            ElapsedMilliseconds = sw.ElapsedMilliseconds;
        }

        /// <summary>
        /// Loads the string table block. This block contains null-terminated strings and is referenced by number of bytes from the start of the string table.
        /// </summary>
        private void LoadStringTable(HeaderSection section)
        {
            StringTable = new Dictionary<long, string>();

            var stringBytes = new ReadOnlySpan<byte>(Bytes, (int)section.AbsoluteStringPosition, (int)section.StringTableSize);
            int start = 0;
            for (int i = 0; i < stringBytes.Length; i++)
            {
                if (stringBytes[i] == 0)
                {
                    var span = stringBytes.Slice(start, i - start);
                    StringTable.Add(start, Encoding.UTF8.GetString(span));
                    start = i + 1;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadCommonData()
        {
            if (Header.CommonDataSize == 0)
            {
                return;
            }

            Reader.BaseStream.Seek(Header.CommonDataOffset, SeekOrigin.Begin);

            for (var i = 0; i < Header.Fields.Count; ++i)
            {
                if (Header.Fields[i].StorageInfo.StorageType == FieldStorageType.InCommonData)
                {
                    var commonData = CommonDataLookup[i] = new Dictionary<uint, object>();
                    uint blockEntries = (Header.Fields[i].StorageInfo.ExternalDataSize / 8);
                    for (var j = 0; j < blockEntries; ++j)
                    {
                        uint recordID = Reader.ReadUInt32();
                        commonData[recordID] = Reader.ReadUInt32();
                    }
                }
            }
        }

        /// <summary>
        /// Loads the non-inline ID block if it exists. This block contains IDs for records.
        /// </summary>
        private bool LoadNonInlineIDs(HeaderSection section)
        {
            if (!Header.HasNonInlineIDs)
            {
                return true;
            }

            NonInlineIDs = new List<uint>((int)section.RecordCount);
            //Reader.BaseStream.Seek(section.AbsoluteIdListPosition, SeekOrigin.Begin);

            //LogDebug($"LoadNonInlineIDs position={section.AbsoluteIdListPosition} count={section.RecordCount}");

            var nonInlineIdSpan = new ReadOnlySpan<byte>(Bytes, (int)section.AbsoluteIdListPosition, (int)section.RecordCount * 4);
            var nonInlineIds = MemoryMarshal.Cast<byte, uint>(nonInlineIdSpan);
            foreach (uint id in nonInlineIds)
            {
                if (id == ENCRYPTED_SENTINEL)
                {
                    return false;
                }
                NonInlineIDs.Add(id);
            }

            return true;
        }

        /// <summary>
        /// Loads the DuplicateID block. This block maps new IDs to original IDs for rows that contain duplicated data.
        /// </summary>
        private bool LoadDuplicateIDs(HeaderSection section)
        {
            Reader.BaseStream.Seek(section.AbsoluteCopyTablePosition, SeekOrigin.Begin);

            uint entries = section.CopyTableSize / 8;
            //LogDebug($"LoadDuplicateIDs position={section.AbsoluteCopyTablePosition} count={entries}");

            for (uint i = 0; i < entries; i++)
            {
                uint newID = Reader.ReadUInt32();
                uint origID = Reader.ReadUInt32();

                if (newID == ENCRYPTED_SENTINEL || origID == ENCRYPTED_SENTINEL)
                {
                    return false;
                }

                if (!IdLookup.TryGetValue(origID, out List<uint> theList))
                {
                    IdLookup[origID] = theList = new List<uint>();
                }

                theList.Add(newID);
            }

            return true;
        }

        /// <summary>
        /// Loads the offset map block. ??
        /// </summary>
        private void LoadOffsetMap(HeaderSection section)
        {
            Reader.BaseStream.Seek(section.AbsoluteOffsetMapPosition, SeekOrigin.Begin);

            LogDebug($"LoadOffsetMap position={section.AbsoluteOffsetMapPosition}");

            uint actualRecordCount = 0;
            NonInlineIDs = new List<uint>((int)section.SparseTableCount);
            SparseOffsets = new List<uint>((int)section.SparseTableCount);
            SparseSizes = new List<ushort>((int)section.SparseTableCount);

            for (uint i = 0; i < section.SparseTableCount; i++)
            {
                uint offset = Reader.ReadUInt32();
                ushort size = Reader.ReadUInt16();

                if (offset != 0 && offset != ENCRYPTED_SENTINEL)
                {
                    if (Header.IsWDC2)
                    {
                        NonInlineIDs.Add(i + Header.MinID);
                    }

                    SparseOffsets.Add(offset);
                    SparseSizes.Add(size);
                    //Console.WriteLine("offset=0x{0:X4} size={1:X4}", offset, size);
                    actualRecordCount++;
                }
            }

            // WDC3 has a new offset ID list structure
            if (Header.IsWDC3)
            {
                Reader.BaseStream.Seek(section.AbsoluteOffsetMapIdListPosition, SeekOrigin.Begin);
                for (uint i = 0; i < section.SparseTableCount; i++)
                {
                    NonInlineIDs.Add(Reader.ReadUInt32());
                }
            }

            section.RecordCount = actualRecordCount;
        }

        /// <summary>
        /// Loads the foreign key map data
        /// </summary>
        private bool LoadForeignKeys(HeaderSection section)
        {
            //ForeignKeyLookup = new Dictionary<uint, uint>();
            if (!Header.HasForeignKeyMap)
            {
                ForeignKeyLookup = new uint[0];
                return true;
            }

            Reader.BaseStream.Seek(section.AbsoluteForeignKeyMapPosition, SeekOrigin.Begin);

            uint numEntries = Reader.ReadUInt32();
            uint colId = Reader.ReadUInt32();
            uint maxForeignId = Reader.ReadUInt32();

            if (numEntries == ENCRYPTED_SENTINEL)
            {
                return false;
            }

            LogDebug($"LoadForeignKeys position={section.AbsoluteForeignKeyMapPosition} numEntries={numEntries} colId={colId} maxForeignId={maxForeignId}");

            ForeignKeyLookup = new uint[numEntries];
            for (uint i = 0; i < numEntries; i++)
            {
                uint foreignId = Reader.ReadUInt32();
                uint recordIndex = Reader.ReadUInt32();
                ForeignKeyLookup[recordIndex] = foreignId;
                //Console.WriteLine("FK {0} => {1}", recordIndex, foreignId);
            }

            return true;
        }

        private void LoadArrayData()
        {
            if (Header.ArrayDataSize == 0)
            {
                return;
            }

            var arraySpan = new ReadOnlySpan<byte>(Bytes, (int)Header.ArrayDataOffset, (int)Header.ArrayDataSize);
            ArrayData = MemoryMarshal.Cast<byte, uint>(arraySpan).ToArray();
            //ArrayData = new BitArray(arraySpan.ToArray());
        }

        /// <summary>
        /// Loads the actual record data.
        /// </summary>
        private void LoadData(HeaderSection section)
        {
            Reader.BaseStream.Seek(section.FileOffset, SeekOrigin.Begin);

            //LogDebug($"LoadData position={section.FileOffset} count={section.RecordCount}");

            // Pre-calculate some things outside the loop
            int fieldCount = Math.Min((int)Header.FieldCount, FieldTypes.Count);
            int recordFieldsSize = fieldCount + (Header.HasNonInlineIDs ? 1 : 0) + (Header.HasForeignKeyMap ? 1 : 0);

            var commonDataFields = new List<CommonDataField>();
            for (byte j = 0; j < FieldTypes.Count; j++)
            {
                var field = Header.Fields[j];
                if (field.StorageInfo.StorageType == FieldStorageType.InCommonData)
                {
                    int noninlineIDBump = (Header.HasNonInlineIDs && j >= IdColumn ? 1 : 0);
                    commonDataFields.Add(new CommonDataField()
                    {
                        FieldIndex = j,
                        RecordFieldsIndex = (byte)(j + noninlineIDBump),
                        DefaultValue = Header.Fields[j].StorageInfo.DataValue,
                        FieldType = FieldTypes[j],
                    });
                }
            }

            var storageTypes = new FieldStorageType[fieldCount];
            var doArrayGroup = new bool[fieldCount];
            for (int j = 0; j < fieldCount; j++)
            {
                FieldStorageInfo fsi = Header.Fields[j].StorageInfo;
                storageTypes[j] = fsi?.StorageType ?? FieldStorageType.InField;
                if (storageTypes[j] == FieldStorageType.InArrayDataGroup)
                {
                    doArrayGroup[j] = (j == 0 || fsi != Header.Fields[j - 1].StorageInfo);
                }
            }

            Reader.SetRecordSize((int)Header.RecordSize);
            for (int i = 0; i < section.RecordCount; i++)
            {
                Reader.StartRecord();
                var recordFields = new List<object>(recordFieldsSize);
                for (int j = 0; j < fieldCount; j++)
                {
                    object rawField = null;
                    int size = Header.Fields[j].Size;
                    FieldStorageInfo fsi = Header.Fields[j].StorageInfo;
                    FieldType fieldType = FieldTypes[j];

                    switch (storageTypes[j])
                    {
                        case FieldStorageType.InCommonData:
                            recordFields.Add(rawField);
                            break;

                        case FieldStorageType.InArrayDataSingle:
                            uint arrayIndexSingle = Reader.GetBits(fsi.FieldSizeBits);
                            uint value = ArrayData[fsi.ExternalDataOffsetArray + arrayIndexSingle];
                            rawField = ConvertUIntToProperType(value, fieldType); /*, fsi.FieldSizeBits);*/ // assume unsigned for now I guess?
                            recordFields.Add(rawField);
                            break;

                        case FieldStorageType.InArrayDataGroup:
                            if (doArrayGroup[j])
                            {
                                uint arrayIndexGroup = Reader.GetBits(fsi.FieldSizeBits);
                                uint groupOffset = fsi.ExternalDataOffsetArray + (arrayIndexGroup * fsi.ArrayCount);
                                for (int k = 0; k < fsi.ArrayCount; k++)
                                {
                                    uint groupValue = ArrayData[groupOffset + k];
                                    rawField = ConvertUIntToProperType(groupValue, fieldType);
                                    recordFields.Add(rawField);
                                }
                            }
                            break;

                        case FieldStorageType.InFieldBitPacked:
                        case FieldStorageType.InFieldBitPacked2:
                            rawField = ConvertUIntToProperType(Reader.GetBits(fsi.FieldSizeBits), fieldType, fsi.FieldSizeBits);
                            recordFields.Add(rawField);
                            break;

                        case FieldStorageType.InField:
                            switch (fieldType)
                            {
                                case FieldType.String:
                                    uint position = Reader.ReadUIntN(size);
                                    if (Header.IsWDC2 || Header.IsWDC3)
                                    {
                                        position = (uint)((Reader.BaseStream.Position - size) + position - Header.StringOffset);
                                    }
                                    rawField = TryGetString(position);
                                    break;

                                case FieldType.Float:
                                    rawField = Reader.ReadSingle();
                                    break;

                                case FieldType.UnsignedInteger:
                                case FieldType.UnsignedInteger16:
                                case FieldType.UnsignedInteger8:
                                    uint temp = Reader.ReadUIntN(size);
                                    // If we forced this field to be read as unsigned, we actually want to convert it back to signed now
                                    if (FieldTypes[j] == FieldType.Integer)
                                    {
                                        rawField = (int)temp;
                                    }
                                    else
                                    {
                                        rawField = temp;
                                    }

                                    break;

                                case FieldType.Integer:
                                case FieldType.Integer16:
                                case FieldType.Integer8:
                                    rawField = Reader.ReadIntN(size);
                                    break;

                                case FieldType.Integer64:
                                    rawField = Reader.ReadInt64();
                                    break;
                            }
                            recordFields.Add(rawField);
                            break;
                    }
                }

                // If the file has non-inline IDs, insert the ID in the correct column
                if (Header.HasNonInlineIDs)
                {
                    recordFields.Insert(IdColumn, NonInlineIDs[i]);
                }

                // If there's non-inline IDs, use those
                uint actualID = 0;
                if (NonInlineIDs != null)
                {
                    actualID = NonInlineIDs[i];
                }
                else
                {
                    object idValue = recordFields[IdColumn];
                    if (idValue is int)
                    {
                        actualID = (uint)(int)idValue;
                    }
                    else if (idValue is uint)
                    {
                        actualID = (uint)idValue;
                    }
                    else if (idValue is long)
                    {
                        actualID = (uint)(long)idValue;
                    }
                }

                if (actualID == ENCRYPTED_SENTINEL)
                {
                    Console.WriteLine("- Skipping probably encrypted entry!");
                    continue;
                }

                // Load common data also, ugh
                foreach (var cdf in commonDataFields)
                {
                    if (!CommonDataLookup.TryGetValue(cdf.FieldIndex, out Dictionary<uint, object> commonData) || !commonData.TryGetValue(actualID, out object existing))
                    {
                        existing = cdf.DefaultValue;
                    }
                    recordFields[cdf.RecordFieldsIndex] = ConvertUIntToProperType((uint)existing, cdf.FieldType);
                }

                // Handle foreign key weirdness
                if (Header.HasForeignKeyMap)
                {
                    recordFields.Add(ForeignKeyLookup[i]);
                }

                Record newRecord = new Record(actualID, recordFields);
                Records.Add(newRecord);

                // WARNING: May have some weird interactions here potentially. FK data should be processed AFTER dupe data. However, it may never end up mattering. Unsure.
                // Handle duplicate IDs
                if (IdLookup.TryGetValue(newRecord.ID, out List<uint> dupes))
                {
                    foreach (uint dupeID in dupes)
                    {
                        var newFields = recordFields.ToList();
                        newFields[IdColumn] = dupeID;
                        Records.Add(new Record(dupeID, newFields));
                    }
                }

                // Seek a little if there's some padding
                Reader.EndRecord();
            }
        }

        private object ConvertUIntToProperType(uint input, FieldType type, uint bits = 0)
        {
            switch (type)
            {
                case FieldType.String:
                    return TryGetString(input);

                case FieldType.Float:
                    return GetFloatFromUInt(input);

                case FieldType.Integer:
                case FieldType.Integer16:
                case FieldType.Integer8:
                    // Mask to the right field size first
                    if (type == FieldType.Integer16)
                    {
                        input = input & ((1 << 16) - 1);
                    }
                    else if (type == FieldType.Integer8)
                    {
                        input = input & ((1 << 8) - 1);
                    }

                    // We can't just directly convert a uintN to an intN if N is smaller than 32. Check for the highest
                    // bit being set and convert to an intN if it is.
                    if (bits > 0)
                    {
                        uint maskValue = (uint)(1 << ((int)bits - 1));
                        if ((input & maskValue) > 0)
                        {
                            uint minusValue = (uint)(1 << (int)bits);
                            return (int)(input - minusValue);
                        }
                    }
                    else if (type == FieldType.Integer16)
                    {
                        return (int)(short)input;
                    }
                    else if (type == FieldType.Integer8)
                    {
                        return (int)(sbyte)input;
                    }

                    return (int)input;

                case FieldType.UnsignedInteger16:
                    input = input & ((1 << 16) - 1);
                    return input;

                case FieldType.UnsignedInteger8:
                    input = input & ((1 << 8) - 1);
                    return input;

                default:
                    return input;
            }
        }

        private object TryGetString(uint position)
        {
            if (position == 0)
            {
                return "";
            }
            else
            {
                if (!StringTable.TryGetValue(position, out string result))
                {
                    result = "[error]";
                }
                return result;
            }
        }

        /// <summary>
        /// Loads the actual data for sparse map files.
        /// </summary>
        private void LoadSparseData(HeaderSection section)
        {
            int fieldCount = FieldTypes.Count + (Header.HasNonInlineIDs ? 1 : 0) + (section.ForeignKeyMapSize > 0 ? 1 : 0);
            for (int i = 0; i < section.RecordCount; i++)
            {
                Reader.BaseStream.Seek(SparseOffsets[i], SeekOrigin.Begin);

                Reader.SetRecordSize(SparseSizes[i]);
                Reader.StartRecord();
                var recordFields = new List<object>(fieldCount)
                {
                    NonInlineIDs[i]
                };
                for (int j = 0; j < FieldTypes.Count; j++)
                {
                    object rawField = null;
                    int size = Header.Fields[j].Size;
                    switch (FieldTypes[j])
                    {
                        case FieldType.String:
                            rawField = Reader.ReadString(false);
                            break;

                        case FieldType.Float:
                            rawField = Reader.ReadSingle();
                            break;

                        case FieldType.UnsignedInteger:
                        case FieldType.UnsignedInteger16:
                        case FieldType.UnsignedInteger8:
                            rawField = Reader.ReadUIntN(size);
                            break;

                        case FieldType.Integer:
                        case FieldType.Integer16:
                        case FieldType.Integer8:
                            rawField = Reader.ReadIntN(size);
                            break;

                        case FieldType.Integer64:
                            rawField = Reader.ReadInt64();
                            break;
                    }
                    recordFields.Add(rawField);
                }
                Reader.EndRecord();

                // If the file has non-inline IDs, insert the ID in the correct column
                /*if (Header.HasNonInlineIDs)
                {
                    recordFields.Insert(IdColumn, NonInlineIDs[i]);
                }*/

                // Handle foreign key weirdness
                if (section.ForeignKeyMapSize > 0)
                {
                    recordFields.Add(ForeignKeyLookup[i]);
                }

                var newRecord = new Record(NonInlineIDs[i], recordFields);
                Records.Add(newRecord);

                // Handle duplicate IDs
                if (IdLookup.TryGetValue(newRecord.ID, out List<uint> dupes))
                {
                    foreach (var dupeID in dupes)
                    {
                        var newFields = recordFields.ToList();
                        newFields[IdColumn] = dupeID;
                        Records.Add(new Record(dupeID, newFields));
                    }
                }
            }
        }

        private void LogDebug(string text)
        {
            if (Debug)
            {
                Console.WriteLine($"[DEBUG] {text}");
            }
        }

        // Shortcut for using BitConverter to go from float to int and vice versa
        [StructLayout(LayoutKind.Explicit)]
        private struct UIntFloat
        {
            [FieldOffset(0)]
            public uint UIntValue;
            [FieldOffset(0)]
            public float FloatValue;
        }
        private object GetFloatFromUInt(uint input)
        {
            return (new UIntFloat { UIntValue = input }).FloatValue;
        }

        // Simple struct to cache common data information
        private struct CommonDataField
        {
            public uint DefaultValue;
            public FieldType FieldType;
            public byte FieldIndex;
            public byte RecordFieldsIndex;
        }
    }
}
