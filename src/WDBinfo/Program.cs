﻿using OpenWoW.LibWowData;
using OpenWoW.LibWowData.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DBinfo
{
    class Program
    {
        static string Separator = new string('-', 80);

        static Dictionary<string, Config> ConfigLookup = new Dictionary<string, Config>();

        static Dictionary<char, FieldType> TypeLookup = new Dictionary<char, FieldType>
        {
            { 'l', FieldType.Integer64 },
            { 'i', FieldType.Integer },
            { 's', FieldType.Integer16 },
            { 'b', FieldType.Integer8 },
            { 'I', FieldType.UnsignedInteger },
            { 'S', FieldType.UnsignedInteger16 },
            { 'B', FieldType.UnsignedInteger8 },
            { 'f', FieldType.Float },
            { 't', FieldType.String },
        };

        static void Main(string[] args)
        {
            bool optionDump = false;
            bool optionCsv = false;
            var filenames = new List<string>();

            // Handle command line arguments
            foreach (string arg in args)
            {
                if (arg == "/dump")
                {
                    optionDump = true;
                }
                else if (arg == "/csv")
                {
                    optionCsv = true;
                }
                else
                {
                    // An entire directory for the brave people
                    if (Directory.Exists(arg))
                    {
                        filenames.AddRange(Directory.GetFiles(arg, "*.db2"));
                    }
                    // A single file
                    else if (File.Exists(arg))
                    {
                        filenames.Add(arg);
                    }
                    // Interpret anything else as a glob attempt
                    else
                    {
                        int i = arg.LastIndexOf(Path.DirectorySeparatorChar);
                        if (i > 0)
                        {
                            filenames.AddRange(Directory.GetFiles(arg.Substring(0, i), arg.Substring(i + 1)));
                        }
                        else
                        {
                            filenames.AddRange(Directory.GetFiles(".", arg));
                        }
                    }
                }
            }

            LoadConfig();

            WowDataFile wdf;
            foreach (string filename in filenames.OrderBy(s => s))
            {
                string filePart = Path.GetFileNameWithoutExtension(filename).ToLowerInvariant();
                if (ConfigLookup.TryGetValue(filePart, out Config config))
                {
                    wdf = new WowDataFile(filename, config.Fields, config.IdColumn);
                }
                else
                {
                    wdf = new WowDataFile(filename);
                }

                wdf.Debug = true;

                bool loadHeaders = !(optionDump || optionCsv);
                try
                {
                    wdf.Load(loadHeaders);
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine(Separator);
                    Console.WriteLine("{0} - ERROR", filename);
                    Console.WriteLine(Separator);
                    Console.WriteLine(ex.ToString());
                    continue;
                }

                Console.WriteLine();
                Console.WriteLine(Separator);
                Console.WriteLine("{0} - {1}ms", filename, wdf.ElapsedMilliseconds);
                Console.WriteLine(Separator);

                // Header
                Console.WriteLine("          Version: {0}", wdf.Header.Version);
                Console.WriteLine("          Records: 0x{0:X2} ({0:n0})", wdf.Header.TotalRecordCount);
                Console.WriteLine("       Raw Fields: 0x{0:X2} ({0:n0})", wdf.Header.FieldCountFromFile);
                Console.WriteLine("    Actual Fields: 0x{0:X2} ({0:n0})", wdf.Header.FieldCount);
                Console.WriteLine("      Record Size: 0x{0:X2} ({0:n0} bytes)", wdf.Header.RecordSize);

                Console.WriteLine("        File Hash: 0x{0:X8}", wdf.Header.TableHash);
                Console.WriteLine("      Layout Hash: 0x{0:X8}", wdf.Header.LayoutHash);
                Console.WriteLine("         Start ID: {0}", wdf.Header.MinID);
                Console.WriteLine("           End ID: {0}", wdf.Header.MaxID);
                Console.WriteLine("           Locale: {0}", wdf.Header.Locale);
                Console.WriteLine("Duplicate ID Size: 0x{0:X8} ({0:n0} bytes)", wdf.Header.DuplicateIdBlockSize);
                Console.WriteLine("            Flags: {0}", GetFlags(wdf.Header.Flags));
                Console.WriteLine("         ID Field: {0}", wdf.Header.IdIndex);
                {
                    Console.WriteLine(" Raw Total Fields: 0x{0:X2} ({0:n0})", wdf.Header.TotalFieldCountFromFile);
                }

                Console.WriteLine("String Table Size: 0x{0:X8} ({0:n0} bytes)", wdf.Header.TotalStringTableSize);
                if (wdf.Header.TotalStringTableSize > 2)
                {
                    Console.WriteLine("String Table Pos : 0x{0:X8}", wdf.Header.StringOffset);
                    Console.WriteLine("String Table End : 0x{0:X8}", wdf.Header.StringOffset + wdf.Header.TotalStringTableSize);
                }

                Console.WriteLine(" Common Data Size: 0x{0:X8} ({0:n0} bytes)", wdf.Header.CommonDataSize);
                if (wdf.Header.CommonDataSize > 0)
                {
                    Console.WriteLine(" Common Data Pos : 0x{0:X8}", wdf.Header.CommonDataOffset);
                    Console.WriteLine(" Common Data End : 0x{0:X8}", wdf.Header.CommonDataOffset + wdf.Header.CommonDataSize);
                }

                Console.WriteLine("  Field Info Size: 0x{0:X8} ({0:n0} bytes)", wdf.Header.FieldStorageInfoSize);

                Console.WriteLine("  Array Data Size: 0x{0:X8} ({0:n0} bytes)", wdf.Header.ArrayDataSize);
                if (wdf.Header.ArrayDataSize > 0)
                {
                    Console.WriteLine("  Array Data Pos : 0x{0:X8} ({0:n0} bytes)", wdf.Header.ArrayDataOffset);
                    Console.WriteLine("  Array Data End : 0x{0:X8} ({0:n0} bytes)", wdf.Header.ArrayDataOffset + wdf.Header.ArrayDataSize);
                }

                /*Console.WriteLine(" Foreign Key Size: 0x{0:X8} ({0:n0} bytes)", wdf.Header.ForeignKeyMapSize);
                if (wdf.Header.ForeignKeyMapSize > 0)
                {
                    Console.WriteLine(" Foreign Key Pos : 0x{0:X8} ({0:n0} bytes)", wdf.Header.ForeignKeyMapOffset);
                    Console.WriteLine(" Foreign Key End : 0x{0:X8} ({0:n0} bytes)", wdf.Header.ForeignKeyMapOffset + wdf.Header.ForeignKeyMapSize);
                }*/

                Console.WriteLine("      Header Size: 0x{0:X2} ({0:n0} bytes)", wdf.Header.HeaderSize);

                // Sections
                for (int i = 0; i < wdf.Header.Sections.Count; i++)
                {
                    var section = wdf.Header.Sections[i];
                    Console.WriteLine(Separator);
                    Console.WriteLine("Section {0}", i + 1);
                    Console.WriteLine(Separator);
                    Console.WriteLine(" Encryption Key Hash : 0x{0:X16}", section.TactKeyHash);
                    Console.WriteLine("         File Offset : 0x{0:X8}", section.FileOffset);
                    Console.WriteLine("        Record Count : 0x{0:X8}", section.RecordCount);
                    Console.WriteLine("   String Table Size : 0x{0:X8} ({0:n0} bytes)", section.StringTableSize);
                    Console.WriteLine(" Sparse Table Offset : 0x{0:X8}", section.SparseTableOffset);
                    Console.WriteLine("        ID List Size : 0x{0:X8} ({0:n0} bytes)", section.IdListSize);
                    Console.WriteLine("         FK Map Size : 0x{0:X8} ({0:n0} bytes)", section.ForeignKeyMapSize);
                    Console.WriteLine("     Copy Table Size : 0x{0:X8} ({0:n0} bytes)", section.CopyTableSize);
                }

                // Field sizes/locations
                Console.WriteLine(Separator);
                for (int i = 0; i < wdf.Header.Fields.Count; i++)
                {
                    var field = wdf.Header.Fields[i];
                    Console.Write("        Field {0,3}: {1} byte{2} @ 0x{3:X2}", i, field.Size, field.Size == 1 ? " " : "s", field.Offset);
                    if (field.ArraySize > 1)
                    {
                        Console.Write("  [{0}]", field.ArraySize);
                    }

                    Console.WriteLine();
                }

                // Padding
                if (wdf.Header.RecordPadding > 0)
                {
                    Console.WriteLine("          Padding: {0} byte{1}", wdf.Header.RecordPadding, wdf.Header.RecordPadding == 1 ? "" : "s");
                }

                // Field storage info
                Console.WriteLine(Separator);
                for (int i = 0; i < wdf.Header.FieldStorages.Count; i++)
                {
                    var fsi = wdf.Header.FieldStorages[i];
                    Console.WriteLine(" Field Storage {0,2}: {1}", i, fsi);
                }

                // Data
                if (optionDump || optionCsv)
                {
                    Console.WriteLine(Separator);
                    Console.WriteLine("Expected field types:");
                    for (int i = 0; i < wdf.FieldTypes.Count; i++)
                    {
                        Console.WriteLine("  {0,2}: {1}", i, wdf.FieldTypes[i].ToString());
                    }
                    Console.WriteLine(Separator);

                    string dumpFile = String.Format("{0}_{1}.csv", wdf.Header.LayoutHash, filePart);
                    StreamWriter dumpWriter = null;
                    if (optionCsv)
                    {
                        Console.WriteLine("Dumping data to {0}", dumpFile);
                        dumpWriter = new StreamWriter(dumpFile);
                    }

                    foreach (var record in wdf.Records ?? Enumerable.Empty<Record>())
                    {
                        var parts = new List<string>();
                        for (int i = 0; i < wdf.FieldTypes.Count; i++)
                        {
                            var field = wdf.FieldTypes[i];

                            if (record.Fields[i] is string)
                            {
                                parts.Add("\"" + (string)record.Fields[i] + "\"");
                                if (field != FieldType.String)
                                {
                                    throw new Exception($"Field { i } is supposed to be a string, not a { field.ToString() }.");
                                }
                            }
                            else if (record.Fields[i] is float)
                            {
                                parts.Add(((float)record.Fields[i]).ToString() + "f");
                                if (field != FieldType.Float)
                                {
                                    throw new Exception($"Field { i } is supposed to be a float, not a { field.ToString() }.");
                                }
                            }
                            else if (record.Fields[i] is int)
                            {
                                parts.Add(((int)record.Fields[i]).ToString() + "i");
                                if (!field.IsInteger())
                                {
                                    throw new Exception($"Field { i } is supposed to be a int, not a { field.ToString() }.");
                                }
                            }
                            else if (record.Fields[i] is uint)
                            {
                                parts.Add(((uint)record.Fields[i]).ToString() + "u");
                                if (!field.IsUnsignedInteger())
                                {
                                    throw new Exception($"Field { i } is supposed to be a uint, not a { field.ToString() }.");
                                }
                            }
                            else if (record.Fields[i] is long)
                            {
                                parts.Add(((long)record.Fields[i]).ToString() + "l");
                                if (field != FieldType.Integer64)
                                {
                                    throw new Exception($"Field { i } is supposed to be a long, not a { field.ToString() }.");
                                }
                            }
                        }

                        if (optionDump)
                        {
                            Console.WriteLine("{0,8} => {1}", record.ID, String.Join(", ", parts));
                        }

                        if (optionCsv)
                        {
                            dumpWriter.WriteLine("{0},{1}", record.ID, String.Join(",", parts));
                        }
                    }

                    if (dumpWriter != null)
                    {
                        dumpWriter.Close();
                        dumpWriter.Dispose();
                    }
                }
            }
        }

        private static void LoadConfig()
        {
            string exePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string csvPath = Path.Combine(Path.GetDirectoryName(exePath), "fileconfig.csv");
            if (File.Exists(csvPath))
            {
                foreach (string line in File.ReadAllLines(csvPath))
                {
                    if (String.IsNullOrWhiteSpace(line) || line.StartsWith("#"))
                    {
                        continue;
                    }

                    var parts = line.Split(',');

                    int idColumn = 0;
                    List<FieldType> fields = null;
                    if (parts.Length >= 2)
                    {
                        idColumn = int.Parse(parts[1]);
                    }

                    if (parts.Length >= 3)
                    {
                        fields = parts[2].Select(c => TypeLookup[c]).ToList();
                    }

                    ConfigLookup[parts[0].ToLowerInvariant()] = new Config
                    {
                        IdColumn = idColumn,
                        Fields = fields,
                    };
                    var types = new List<FieldType>();
                }
            }
        }

        static string GetFlags(uint flags)
        {
            string ret = String.Format("0x{0:x}", flags);
            foreach (Flag flag in Enum.GetValues(typeof(Flag)))
            {
                if ((flags & (uint)flag) > 0)
                {
                    ret += " " + flag.ToString();
                }
            }
            return ret;
        }
    }

    class Config
    {
        public int IdColumn;
        public List<FieldType> Fields;
    }
}

